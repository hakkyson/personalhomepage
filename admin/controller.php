<?php require_once('../database.php'); ?>
<?php $id_update = $_POST[id]; ?>
<?php $id_delete = array_keys($_POST)[0]; ?>
<?php $shift = array_shift($_POST); ?>
<?php if ((strnatcasecmp($shift, "delete") + 1) && (count($_POST)) <= 1) {
            $arr = [
                ':id' => $id_delete,
            ];
            $db->insert("DELETE FROM messages WHERE id=:id", $arr);
    header('Location: /admin/index.php');
    }
?>
<?php if (strnatcasecmp($shift, "Submit") && (count($_POST)) > 1) {
    $array = [
        ':id' => $id_update,
        ':name' => $_POST[InputName],
        ':email' => $_POST[InputEmail],
        ':message' => $_POST[InputMessage],
    ];
    $db->insert("UPDATE messages SET name=:name,email=:email,message=:message WHERE id=:id", $array);
    header('Location: /admin/index.php');
}
?>