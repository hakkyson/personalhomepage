<?php require_once('../database.php'); ?>
<?php
$array = $db->query("SELECT * FROM messages");
$key = array_keys($_POST);
foreach($array as $records){
    if ($records[id] == $key[0]) $record = $records;
}
?>
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <title>Update</title>
<?php require_once('../header.php'); ?>

    <div class="row">
        <div class="col-xs-2">
        </div>
        <div class="col-xs-8">

            <form role="form" action="/admin/controller.php" method="post">
                <div class="form-group">
                    <input type="hidden" class="form-control" name="id" id="id" placeholder="id"
                           value="<?php echo $key[0]; ?>">
                    <label for="InputName">Your Name</label>
                    <div class="input-group">
                        <input class="form-control" name="InputName" id="InputName" placeholder="Enter Name"
                               value="<?php echo $record[name]; ?>">
                        <span class="input-group-addon"></span></div>
                </div>
                <div class="form-group">
                    <label for="InputEmail">Your Email</label>
                    <div class="input-group">
                        <input class="form-control" id="InputEmail" name="InputEmail" placeholder="Enter Email"
                               value="<?php echo $record[email]; ?>">
          <span class="input-group-addon"><?php
              if ($_POST[email_error]) echo "Введите корректный e-mail адресс";
              ?></span></div>
                </div>
                <div class="form-group">
                    <label for="InputMessage">Message</label>
                    <div class="input-group">
          <textarea name="InputMessage" id="InputMessage" class="form-control" rows="5" placeholder="Enter Message"><?php
              echo $record[message];
              ?></textarea>
          <span class="input-group-addon"><?php
              if ($_POST[message_error]) echo "Введите сообщение не более 500 символов";
              ?></span></div>
                </div>
                <input type="submit" name="submit" id="submit" value="Submit" class="btn btn-info pull-right">
            </form>

        </div>
        <div class="col-xs-2">
        </div>
    </div>

<?php require_once('../footer.php'); ?>