<?PHP
session_start();
if ( isset($_POST[logout]) ) unset( $_SESSION["login"] );
// Если пользователь не авторизован
if ( !isset($_SESSION["login"]) )
{
    // Если форма для ввода логина и пароля была заполнена
    if ( isset($_POST["auth"]) )
    {
        $login = '123';
        $password = '123';
        if( ($_POST['login']==$login) && ($_POST['password']==$password) )
        {
            // авторизация прошла успешно
            $_SESSION['login'] = $_POST['login'];
            header( "Location: ".$_SERVER["PHP_SELF"] );
        }
        else header('Location: /admin/index.php');
    }
    else
    {
        echo '<!DOCTYPE html><html lang="en"><head><title>Login</title>';
        require_once("../header.php");
        echo '<div class="row"><div class="col-xs-2"></div><div class="col-xs-8">';
        echo '<form name="authForm" method="post" action="'.$_SERVER["PHP_SELF"].'">';
        echo 'Логин: <input type="text" name="login" value="" class="form-control"><br/>';
        echo 'Пароль: <input type="password" name="password" value="" class="form-control"><br/>';
        echo '<input type="submit" name="auth" value="Вход" class="btn btn-info pull-right">';
        echo '</form>';
        echo '</div><div class="col-xs-2"></div></div>';
        require_once('../footer.php');
    }
    die();
}
?>
<?php require_once('../database.php'); ?>
<?php
    $array = $db->query("SELECT * FROM messages");
?>
    <!DOCTYPE html>
    <html lang="en">
<head>
    <title>Admin</title>
<?php require_once('../header.php'); ?>

<div class="row">
    <div class="col-xs-2"></div>
    <div class="col-xs-8">

    </div>
    <div class="col-xs-2">
        <form role="form" action="/admin/index.php" method="post">
            <input type="submit" name="logout" value="logout" class="btn btn-info">
        </form>
    </div>
</div>

    <div class="row">
        <div class="col-xs-2">
        </div>
        <div class="col-xs-8">
            <table class="table">
                <thead>
                <tr>
                    <th>id</th>
                    <th>name</th>
                    <th>email</th>
                    <th>message</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($array as $record): ?>
                <tr>
                    <td><?php echo $record[id]; ?></td>
                    <td><?php echo $record[name]; ?></td>
                    <td><?php echo $record[email]; ?></td>
                    <td><?php echo $record[message]; ?></td>
                    <td>
                        <form role="form" action="/admin/update.php" method="post">
                            <input type="submit" name="<?php echo $record[id]; ?>" id="submit" value="update" class="btn btn-info pull-right">
                        </form>
                    </td>
                    <td>
                        <form role="form" action="/admin/controller.php" method="post">
                        <input type="submit" name="<?php echo $record[id]; ?>" id="submit" value="delete" class="btn btn-info pull-right">
                        </form>
                    </td>
                </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
            <ul class="pagination">
                <li><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li class="disabled"><a href="#">4</a></li>
                <li><a href="#">5</a></li>
            </ul>
        </div>
        <div class="col-xs-2">

        </div>
    </div>
<?php require_once('../footer.php'); ?>