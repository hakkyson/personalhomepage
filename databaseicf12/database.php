<?php

$config = [
    'host' => 'localhost',
    'user' => 'root',
    'password' => '',
    'database' => 'personaldb',
];

$db = new DataBase($config);

class DataBase
{
    private $pdo;

    public function __construct($config = null) {
        $this->connect($config[host],$config[database],$config[user],$config[password]);
    }

    public function __destruct() {
        $this->pdo = null;
    }

    public function query($sql = '') {
        return $this->pdo->query($sql)->fetchAll();
    }

     public function insert($sql = '', $array = []) {
        return  $this->pdo->prepare($sql)->execute($array);
    }

    private function connect($host, $dbname, $user, $password) {
        $this->pdo = new PDO('mysql:host='.$host.';dbname='.$dbname, $user, $password);
    }
}