<?php require_once('/database.php'); ?>
<?php
    if (!empty($_POST)) {
        if (!filter_var($_POST[InputEmail], FILTER_VALIDATE_EMAIL)) {
          $_POST[email_error] = true;
        } else
        if (empty($_POST[InputMessage]) || (strlen($_POST[InputMessage]) > 500)) {
          $_POST[message_error] = true;
        }
        else
        {
        $_POST[post] = true;
        $fp = fopen('data.csv', 'a');
        $_POST[record] = fwrite($fp, $_POST[InputName].','.$_POST[InputEmail].','.$_POST[InputMessage]."\n");
        fclose($fp);
            $array = [
                ':name' => $_POST[InputName],
                ':email' => $_POST[InputEmail],
                ':message' => $_POST[InputMessage],
            ];
            $db->insert("INSERT INTO developertest.messages (id, name, email, message)
              VALUES (NULL, :name, :email, :message)", $array);
        }
      } else {
        $_POST[post] = false;
      }
  ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Contacts</title>
<?php require_once('/header.php'); ?>
<div class="row">
  <div class="col-xs-2"></div>
  <div class="col-xs-8">

  </div>
  <div class="col-xs-2"></div>  
</div>
<div class="row">
<div class="col-xs-2">
</div>
<div class="col-xs-8">
<form role="form" action="" method="post">
      <div class="form-group">
        <label for="InputName">Your Name</label>
        <div class="input-group">
          <input class="form-control" name="InputName" id="InputName" placeholder="Enter Name"
          value="<?php if (!$_POST[post]) echo $_POST[InputName]; ?>">
          <span class="input-group-addon"></span></div>
      </div>
      <div class="form-group">
        <label for="InputEmail">Your Email</label>
        <div class="input-group">
          <input class="form-control" id="InputEmail" name="InputEmail" placeholder="Enter Email" 
          value="<?php if (!$_POST[post]) echo $_POST[InputEmail]; ?>">
          <span class="input-group-addon"><?php 
            if ($_POST[email_error]) echo "Введите корректный e-mail адресс";
          ?></span></div>
      </div>
      <div class="form-group">
        <label for="InputMessage">Message</label>
        <div class="input-group">
          <textarea name="InputMessage" id="InputMessage" class="form-control" rows="5" placeholder="Enter Message"><?php
            if (!$_POST[post]) echo $_POST[InputMessage];
          ?></textarea>
          <span class="input-group-addon"><?php
            if ($_POST[message_error]) echo "Введите сообщение не более 500 символов";
          ?></span></div>
      </div>
      <input type="submit" name="submit" id="submit" value="Submit" class="btn btn-info pull-right">   
  </form>
  </div>
  <div class="col-xs-2">
  </div>
</div>

<?php require_once('/footer.php'); ?>